"""
.. list-table::
    :header-rows: 1
    :widths: auto

    * - Import alias
      - Maps to
    * - :class:`!quantify.instrument_coordinator.components.InstrumentCoordinatorComponentBase`
      - :class:`.InstrumentCoordinatorComponentBase`
"""

from .base import InstrumentCoordinatorComponentBase

__all__ = ["InstrumentCoordinatorComponentBase"]
